<?php

return [

	'default' => [
		new \App\Breadcrumbs\HomeBreadcrumb,
	],

	'classes' => [
		'container' => 'breadcrumb-container',
		'list' => 'breadcrumb-list',
		'item' => 'breadcrumb-item-class',
		'link' => 'breadcrumb-link-class',
		'icon' => 'breadcrumb-icon-class',
	],
];
